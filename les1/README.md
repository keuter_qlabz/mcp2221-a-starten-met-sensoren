# Les 1 - Klaarzetten van de omgeving

Als eerste ga je na of alles wat je nodig hebt op je laptop staat. Deze uitleg zal Linux specifiek zijn, omdat ik daar het meeste ervaring mee heb. De distributie is daarbij liefst Debian of Ubuntu. Die zijn allebei erg populair en voor heel veel dingen in te zetten, van de kleinste tot grootste computersystemen. 

Als eerste zorg je dat je van alles wat al op je laptop staat je de laatste versie hebt, hiervoor open je een terminal en type je volgende:

`sudo apt update && sudo apt full-upgrade -y` gevolgd door Enter.

## Installeren van Python3, de CircuitPython libraries en de Mu-Editor
Waarom de keuze voor Python? Python is een van de populairste ontwikkeltalen ter wereld. Het is een voor mensen goed leesbare en begrijpbare taal en is daarom veel gebruikt als kennismaking met programmeren. Maar vergis je niet, het is ook een van de meest breed ingezette programmeer talen, van data analyse tot het bouwen van websites en ook moderne ontwikkelingen zoals het draaien van kleine programma's in de cloud. Daarom is het nu interessant om met Python aan de slag te gaan om met sensor data te werken, je kan Python dan inzetten aan de rand om de data te verzamalen en Python in de data analyse en het bouwen van dashboards, waar je de status van bijvoorbeeld je kamerplant kan zien..

### 1.1 - Python 3
Hoogst waarschijnlijk heb je op Ubuntu al Python3 geinstalleerd staan. Dit kan in een tweetal smaken zijn, dus je moet eerst even controlleren of je Python3 op je computer hebt staan:

`python --version` of
`python3 --version`

Mocht er foutmelding bij de laatste komen, dan staat Python3 nog niet op je computer, om dit te installeren type je het volgende:

`sudo apt install python3`

Dit installeert de laatste Python3 versie van je Linux distributie op je computer. 

Hierna moet je nog twee libraries installeren om via de USB poort van laptop met de MCP2221 te communiceren en verder nog wat software van Adafruit waarmee je in Python kan programmeren voor de sensoren die we gaan aansluiten.

`sudo apt install libusb-1.0 libudev-dev`

Maak om gebruik te maken van deze libraries het volgende bestand aan **/etc/udev/rules.d/99-mcp2221.rules**, met daarin de volgende tekst:

> SUBSYSTEM=="usb", ATTRS{idVendor}=="04d8", ATTR{idProduct}=="00dd", MODE="0666" 

`sudo nano /etc/udev/rules.d/99-mcp2221.rules`
Kopieer de regel erin en sluit af met **CTRL-X** en **Y**

Zoals je hieronder ziet, helpen al deze libraries samen om via de MCP2221 te praten met een hele set van sensoren en LED's, via allerlei soorten verbindingen (I2C, ADC, DAC, GPIO). Die verbindingen noemen we ook wel interfaces. Deze verbindingen komen allemaal vandaag voorbij, maar het is goed af en toe dit plaatje erbij te halen om te zien waar je mee bezig bent: 
![overzicht](pictures/sensors_usb_to_mcp2221.png)

Om het installeren van de Python libraries makkelijker te maken gebruik je een python package manager, pip3. Deze installeer je met het volgende commando:

`sudo apt install python3-pip`

en daarna installeren we de python libraries:

`pip3 install hidapi`

`pip3 install adafruit-blinka`


Telkens als je met de mcp2221 wil communiceren moet je controlleren of er een systeem variabele gezet is voor de variabele **BLINKA_MCP2221**, die wordt namelijk door adafruit-blinka gebruikt om te weten welk apparaat aangesloten is (in dit geval dus de MCP2221).

`export BLINKA_MCP2221="1"`

om te controlleren type je:

`echo $BLINKA_MCP2221` 

wat als het goed is de waarde **1** teruggeeft.

### 1.2 - CircuitPython Libraries testen
Om te testen of alles goed is gegaan bij het opzetten test je of je kan verbinden met de MCP2221, hiervoor hoef je behalve dit bordje niets aan te sluiten. Steek de USB-C connector in de MCP2221 en de andere kant in je laptop, als het goed is gaat er een groen ledje branden.

Start hierna de Python interactieve omgeving op door het volgende te typen:
`python3` gevolgd door Enter

Je bent nu in de Python REPL, de omgeving waarin je python opdrachten meteen kan uitvoeren, om bijvoorbeeld zoals nu te testen of je connectie kan maken met de MCP2221.

Type de volgende commando's en kijk of er een foutmelding ontstaat, is dat niet het geval, dan werkt alles dus goed:
```
import board
dir(board)
```
Dit laatste commando levert als antwoord o.a. alle interfaces van de MCP2221 terug, die gaan we tijdens de volgende lessen uitgebreid bespreken. 

### 1.3 - Mu-Editor
De Mu-Editor is een simpele, gebruiksvriendelijke programmeer omgeving, gespecialisseerd voor het maken van kleine projecten voor leren werken met bijvoorbeeld microprocessoren en sensoren zoals bijvoorbeeld die van Adafruit, Raspberry Pi of de BBC Micro:bit.

![mu_logo](pictures/Mu-logo.png) 

In Debian en Ubuntu is dit heel simpel:

`sudo apt install mu-editor`

Start het programma en selecteer Python3 bij het opstart schermpje. 

### Instellen omgevings variabelen
Om ervoor te zorgen dat we telkens als we de Mu editor gebruiken de juiste variabele beschikbaar hebben (de BLINKA_MCP2221="1") van eerder in de les, plaatsen we deze in een speciaal veld in Mu. Helemaal rechts onderin de editor zie naast het woord Python een tandwieltje, klik hierop en ga naar het tweede tabblad "Python3 Environment" en type op de eerste regel:

`BLINKA_MCP2221="1"` en klik daarna op OK.

Ook in deze editor zit de REPL interactieve Python omgeving. Druk op de knop REPL en type wat we eerder in de standaard REPL in de terminal getypt hebben.

```
import board
dir(board)
```

Als het goed is krijg hetzelfde antwoord als eerst.

[**Door naar les 2!**](les2)


