# Analoge waardes uitlezen

Je kan met de MCP2221 ook Analog apparaten aansluiten, besturen en uitlezen. Hiervoor heeft de de MCP2221 zogenaamde ADC en DAC functies. ADC is een afkorting voor het vertalen van een Analoog signaal (A) naar een Digitaal (D) singnaal, dus Analog to Digital Converter (ADC) in het Engels. Hetzelfde maar dan omgekeerd heet DAC, dus een Digital to Analog Converter (DAC). Het digitale singaal bestaat uit waardes van 0 of 1 en bij het analoge signaal zijn er veel meer waardes mogelijk. De onderstaande grafieken laten zien hoe een analoog singnaal (boven) over de tijd in heel geleidelijk verandert, terwijl het digitale signaal (onder) telkens maar een waarde van 1 of 0 heeft:

![wave_forms](pictures/Sin-Square_wave-form.png)

De MCP2221 kan deze signalen met elkaar vergelijken en doorgeven op een manier dat je hier vanuit beide werelden (de analoge en de digitale) iets mee kan doen. Je gaat dit zelf in de oefening hieronder doen door een electrisch circuit te bouwen op een Breadboard, met in het circuit een draaiknop (een analoge weerstand). Door aan de knop te draaien gaat er meer of minder stroom door het circuit, wat er voor zorgt dat de MCP2221 telkens een andere waarde uitleest (hoger of lager dan de vorige waarde). Deze waarde vertaal je in je programma weer tot waardes in een grafiek.  

## 3.1 Draaiknop uitlezen
Het MCP2221 bordje koppel je in deze oefening aan een draaiknop (een potentio meter), die geeft dan waarde terug die we op een grafiek kunnen zetten.

![analog](pictures/sensors_gpio_adc_bb.png)


## 3.2 Adafruit IO dashboard
Het Adafruit IO dashboard kan je net als in les 2 gebruiken om iets met de waardes van de draaiknop te doen. We gaan hiervoor een het Adafruit IO Dashboard gebruiken. Dit dashboard kan je gebruiken om een website te maken waarop je het draaien aan knop direct op Internet zichtbaar maakt. Hiervoor gebruiken we  

![draaiknop_IO](pictures/draaiknop_IO.png)

## 3.3 Waardes weergeven op Internet

