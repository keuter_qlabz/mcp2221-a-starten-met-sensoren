# MCP2221-A starten met sensoren
Misschien heb je je wel eens afgevraagd hoe bijvoorbeeld de microfoon in je telefoon werkt, hoe je telefoon merkt dat je het scherm draait en automatisch het beeld aanpast, of hoe een weerstation de luchtdruk en temperatuur meet en een hoogtemeter weet hoe hoog je zit. Voor al die dingen hebben we de afgelopen eeuwen sensoren gebouwd, een bekend voorbeeld is de thermometer. Tegenwoordig zijn veel van die sensoren heel klein en kunnen ze ingebouwd worden in bijvoorbeeld telefoons, die de wereld om ons heen kunnen uitlezen en dit kunnen doorgeven als een digitaal signaal. Heel veel van deze sensoren zitten tegenwoordig ook in auto's en zelfs koelkasten en zijn verbonden met het Internet. Hierdoor is het mogelijk dat je op afstand weet wat de temperatuur is in je koelkast en je dit zou kunnen aanpassen of dat de garage weet dat er storing is in de auto waarvoor je binnenkort langs moet komen. 

Deze GitLab omgeving heb ik gemaakt om te leren hoe je dit soort dingen zelf kan bouwen en te begrijpen hoe het werkt. Hiervoor gebruik ik de MCP2221 van Adafruit, een bordje dat je met een USB stekker verbindt aan je computer en waarmee je sensoren kan uitlezen en aansturen. De lessen zijn deels op basis van [het Adafruit learn programma](https://learn.adafruit.com/circuitpython-libraries-on-any-computer-with-mcp2221?view=all), inclusief code en uitleg, maar dan in het Nederlands. Soms zijn de lessen aangepast naar het gebruik met de MCP2221 en of de programma's op de computer zoals Python, Mu-editor, Google Docs of Jupyter Notebooks.

## Opbouw lessen
*  [les 1 ](les1)gaat over het klaarzetten en testen van alle onderdelen, het installeren van de software op je computer. Je krijgt uitleg over de sensoren en wat je er mee kan maken.
*  [les 2](les2) gaat over het gebruik van de I2C connectoren, het protocol en hoe je dit vanuit de mu-editor allemaal in Python programmeert.
*  [les 3 ](les3)gaat over analoge connecties maken en waarden uitlezen hiervoor gebruiken de potentiometer (de draaiknop) en de mogelijkheden voor ADC/DAC van het MCP2221 bordje.
*  [les 4](les4) gaat over de GPIO poortjes en hoe je bijvoorbeeld een LED-lampje kan laten branden.
*  [les 5](les5) alles combineren... 

Elke les duurt ongeveer een uur, waarvan je het grootste deel zelf met de spullen aan de slag gaat.

## Wat heb je nodig..
In het pakket zitten de volgende zaken:

* MCP2221, incl. 4 STEMMA/QT QWIIC kabels van verschillende lengtes
* 4 sensoren:
* LSM6DS33
* DPS310
* PCT2075
* VCNL4040
* Adafruit STEMMA Speaker,  incl. STEMMA kabel
* OLED schermpje 128x64, I2C
* Breadboard, incl. kabeltjes van verschillende lengtes
* USB-C kabel
* 5mmm LED lampjes (2x groen, blauw, geel, rood en wit)
* Drukknopjes (2x groot, 2x klein, 1x micro)
* Lichtweerstandje
* Weerstanden (5x 1kOhm, 5x 330Ohm, 6x 10kOhm)



Een PC (liefst Linux) met daarop:
*  Python 3
*  muEditor
*  de python libraries van de MCP2221 en de sensoren 
*  afhankelijk van de tutorials Jupyter Notebook of Google docs software

![hardware](pictures/FXT23871.JPG)

In de les installeren we alles zelf, dus je hoeft alleen een werkende Linux laptop bij je te hebben.

## Waarom Adafruit MCP2221
Het grote voordeel van deze connector is dat je er direct mee aan de slag kan vanaf je laptop en je op een laagdrempelige manier toegang krijgt tot de wereld van sensoren. Deze manier verschilt niet heel veel van de het maken van code voor Arduino, de CircuitPython bordjes van o.a. Adafuit of de Raspberry Pi. Daardoor is de code die je maakt ook snel over te zetten naar deze CircuitPython of MicroPython geschikte microprocessor gebaseerde bordjes. Verder levert aansluiten op de PC nog extra voordeel op, zo kan je programatuur draaien op je PC die te zwaar is voor de kleine bordjes. Programma's die bijvoorbeeld mooie grafieken of interactieve websites publiceren op basis van je sensor data. Er zijn meer van dit soort connectoren, het voordeel van deze van Adafruit is dat er veel voorbeeld code voor is en er veel mensen actief projecten mee bouwen.
