# Les 5: Alles combineren

Nu ben je klaar om zelf iets te bouwen, hiervoor krijg je een voorbeeld met een klein OLED schermpje waardoor je direct waardes kan aflezen. Dit is de eerste stap in de richting van een klein zelfstandig project, waarbij je kan besluiten dat je dit gaat bouwen met een kleine onafhankelijke microprocessor zoals de CircuitPython M4 bordjes van Adafruit. Voordeel daarvan is dat je niet meer de hele tijd je laptop aan hoeft te laten staan, maar alleen een heel kleine energie zuinige computer. Maar eerst gaan we verder aan de slag met de MCP2221, aangesloten op je laptop.

## Les 5.1: Adafruit CircuitPython gebruiken
Hoe bestuur je een scherm, met een protocol dat een vaste (lage) snelheid heeft zoals I2C? Hoe zoek je de passende libraries en hoe zorg je dat je code werkt? Dit zijn de belangrijkste dingen die je vandaag kan leren, hoe kan een sensor gebruiken in mijn eigen project en hoe ga ik aan de slag.

Veel projecten op Adafruit hebben een lijstje met hardware die je kan gebruiken, maar dit is niet altijd de hardware die je in huis hebt. Gelukkig is de code zoals we die ook in de lessen gebruikt hebben met verschillende hardware te gebruiken. Waar je op moet letten is volgende:
*  Welk protocol/interface wordt ingezet, is het I2C, GPIO of worden er analoge waardes uitgelezen?
*  In de voorbeeld Python code, wordt daar uitgelegd hoe de code werkt, bestudeer dit goed en let vooral ook op namen van poorten. Deze kunnen verschillen van de poort-namen op de MCP2221.
*  Kijk goed naar de aansluitingen en zorg ervoor dat je de hardware goed op elkaar aansluit, zeker als je geen gebruik kan maken van de qwiic/STEMMA QT stekkertjes.
*  Als er gesproken wordt over pull-up weerstanden, zorg dan dat je die in je circuit opneemt, teken het circuit eerst om gevoel te krijgen bij wat je gaat bouwen (meer hierover bij de uitleg voor Fritzing een programma dat je helpt de schema's te tekenen op de computer).
*  Zorg voor veiligheid, zorg dat je werkplek ruimte biedt en dat je voordat je de stekker erin doet eerst alles aangesloten hebt. Als je merkt dat iets niet werkt, **EERST DE STEKKER ERUIT**, dan pas je circuit nakijken.
*  


## Les 5.2 Eerst een use-case bedenken
Je zou kunnen beginnen met verzinnen wat je zou willen maken. Is het iets voor school, wil ik thuis automtiseren, wil ik iets maken dat mij helpt, wil ik een waarschuwing krijgen wanneer iets gebeurt? Stel je gaat uit van de sensoren die je hebt, wat kan je daarmee maken:

We hebben de barometer/temperatuur meter, de accelo/gyro meter, het OLED schermpje, de MCP2221 die allerlei analoge waardes uit kan lezen, verder nog wat ledjes, speakertje, licht sensoren en draai- en druk-knoppen en Adafruit IO. Als je alleen al deze dingen al hebt zijn er heel veel mogelijkheden.