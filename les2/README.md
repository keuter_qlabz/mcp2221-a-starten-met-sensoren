# Les 2 De MCP2221, introductie hardware en I2C protocol

![i2clogo](pictures/I_C_bus_logo.svg.png)

I2C is een manier om met hardware te praten, zonder dat deze hardware heel veel eigen zware computerkracht nodig heeft om dit te kunnen. Dit protocol is ooit uitgevonden door Philips (al in 1982). Het wordt nog steeds gebruikt om op relatief lage snelheid waardes uit lezen bij sensoren of om een waarde in te stellen op de aangesloten hardware.

De MCP2221 van Adafruit maakt het werken op basis van het I2C protocol met de juiste sensoren nog eenvoudiger. Met de juiste kabeltjes kan je direct de sensor aansluiten op de computer en hiervoor heb je maar 1 USB poort nodig op je computer. Adafruit heeft al deze componenten uitgezocht en werkend gemaakt voor het programmeren met Python. Je kan ook andere sensoren proberen aan te sluiten, maar dan moet je vaak gaan solderen en moet je op zoek naar geschikte drivers om vanuit Python met de sensor te kunnen praten.

Als eerste een korte uitleg over de hardware van de MCP2221:

![mcp2221](pictures/MCP2221_Fritzing.png)

Links zit de USB-C connector voor aansluiting op je laptop, rechts zit een qwiic/STEMMA QT aansluiting (de kleine zwarte aansluiting aan de rechter kant). Op de andere sensoren vind je twee STEMMA QT aansluitingen, waarbij als de tekst rechtopstaat op de linker aansluiting de inkomende stekker en de rechter de uitgaande aansluiting is. Voor deze aansluitingen heb je allemaal kabeltjes beschikbaar en een werkende opzet kan als volgt uitzien:

![chain](pictures/Computer_and_I2C_Sensors.png)

Je gaat nu één sensor (bijvoorbeeld de PCT2075) op de MCP2221 aansluiten. De kabeltjes kunnen telkens maar op een manier in en uit de sensor, let goed op de stekker connectie (de kleine gaatjes) altijd bovenaan zitten, bij aansluiten hoef je geen grote kracht te zetten. **Let dus op, kabeltjes kunnnen maar op één manier aangesloten worden, als je teveel kracht moet zetten, dan doe je het waarschijnlijk verkeerd om!** Zoals op het plaatje afgebeeld zie je dat er een draai in kabeletje zit, dat is goed.

![mcp2221-pct2075](pictures/MCP2221_PCT2075_Fritzing.png)


Sluit hierna met de USB kabel de MCP2221 op de PC aan. De stroom uit de USB van je PC zal de MCP en aangesloten sensor starten, en je ziet de LEDjes op de boardjes groen oplichten. Hierna kan je de USB stekker weer uit je laptop halen en de sensoren ook weer van elkaar loshalen.

## Les 2.1 Het I2C protocol
Het grote voordeel van I2C is dat je met maar twee draadjes gebruikt voor het versturen en ontvangen van gegevens. Je kan een hele reeks van sensoren aanspreken op dezelfde twee aansluitingen. Je ziet als je goed kijkt wel 4 kabeltjes lopen, maar twee hiervan zijn voor de electrische stroom. Doordat de sensoren in serie gezet kunnen worden (dat betekent achter elkaar zetten), kan je met heel weinig poorten op de hoofdcomputer (in dit geval de twee poorten MCP2221) heel veel sensoren aansluiten. Voorwaarde is dat deze sensoren wel allemaal een eigen adres hebben, eigenlijk net zoals bij jou in de straat.

![serialbus](pictures/Serial_bus.png)

De pakketbezorger zal geen moeite hebben met bezorgen op huisnummers 1 en 2, maar bij 3 heeft hij een probleem. Zo werkt dit ook bij de I2C bus en dit komt snel voor als je dezelfde sensor twee of meer keren op de bus (de ketting van sensoren) plaatst. Al deze sensoren hebben dan hetzelfde adres. Bij de sensoren die je vandaag gebruikt zal dit conflict niet optreden, maar als het wel zo is, moet je vaak in de hardware van de sensor iets wijzigen. Hoe dit werkt zal je zien in les 5 en is voor nu nog niet nodig.

## Les 2.2 Programeren met I2C en Python
De eerste sensor die we gaan gebruiken is een beweging sensor, de LSM6DS33. Deze sensor kan over de x,y,z-assen aangeven dat de sensor draait en hoe de sensor over die assen versnelt. Als eerste sluit je de sensor aan op MCP2221 door de kabeltjes op de juiste manier aan te sluiten, zorg dat je de sensor en MCP2221 op dezelfde wijze me de tekst leesbaar zoals in de afbeelding neerlegd. **Let weer op, kabeltjes kunnnen maar op één manier aangesloten worden, als je teveel kracht moet zetten, dan doe je het waarschijnlijk verkeerd om!** Zoals op het plaatje afgebeeld zie je dat er een draai in kabeletje zit, dat is goed.

![mcp-lsm](pictures/MPC2221_LSM6DS33_Fritzing.png)

Om met de sensor te kunnen praten vanuit de laptop, moet je eerst de juiste python libraries installeren. Dit doe je door het volgende commando uit te voeren:

`pip3 install adafruit-circuitpython-lsm6ds`

Sluit nu de MCP2221 met de USB kabel aan op je PC, als het goed is lichten de groene ledjes op de MCP2221 en de aangesloten LSM6DS33 sensor op. 

Hierna kan je in de Mu editor omgeving een bestand aanmaken met de naam accel_test.py, met hierin de volgende code:

```
import time
import board
import busio
from adafruit_lsm6ds import LSM6DS33
 
i2c = busio.I2C(board.SCL, board.SDA)
 
sensor = LSM6DS33(i2c)
 
while True:
    print("(%.2f, %.2f, %.2f)" % (sensor.acceleration))
    print("(%.2f, %.2f, %.2f)" % (sensor.gyro))
    time.sleep(0.5)
```

Zorg dat je het REPL venster onderaan open hebt staan (klik anders de REPL knop in en je ziet het venster onderaan opgaan). Daarna klik op run, dan duurt het nog een paar seconden en je ziet de eerste data al voorbij komen in het REPL venster. Beweeg nu de sensor en je ziet de getallen veranderen. Als je deze data in een grafiek wil stoppen, dan klik je op Plotter en je ziet na een paar seconden een grafiek voorbij komen.

## Les 2.3 Barometer en Temperatuur
In deze les maken we gebruik van een sensor die twee waarden levert. Deze waarden kunnen we dan online zetten waardoor we over de dag heen inzicht krijgen in de temperatuur wijziging, je zou dit nog kunnen aanvullen met de buitentemperatuur en of het zonnig of bewolkt is, zoals online gemeten. Hiervoor gebruik je Adafruit IO, dit is een cloud omgeving waar je IoT data kan opslaan en waarvoor Adafruit ook libraries voor Python heeft gemaakt. Hierdoor kan je in hetzelfde programma dat je gebruikt om je sensor data te lezen, meteen deze data ook verwerken en in de Adafruit IO cloud te zetten.

Hiervoor installeer je eerst de bendoigde Python libraries met Pip3:

`pip3 install adafruit-io`




[Verder naar Les 3..](les3)
